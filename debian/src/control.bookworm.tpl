Package: apt-tray-notifier
Version: {VERSION}~bookworm
Section: utils
Priority: optional
Architecture: all
Depends: python3, python3-pyqt5, apt
Recommends: xterm
Maintainer: Tomas Åkesson <tomas@entropic.se>
Homepage: https://gitlab.com/entropic-software/apt-tray-notifier
Description: Notifies about Debian updates
 Shows a notification in the system tray when updates are available
