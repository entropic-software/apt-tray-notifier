#!/usr/bin/env python3

# Copyright 2021 Tomas Åkesson <tomas@entropic.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import subprocess
import sys
import time

try:
    from PyQt5.QtCore import QTimer
    from PyQt5.QtGui import QIcon
    from PyQt5.QtWidgets import QApplication, QSystemTrayIcon, QMenu, QWidget
    from PyQt5.QtWidgets import QDialog, QMessageBox
except Exception:
    print("Qt5 not found. Please install it.")
    sys.exit(1)


APP_NAME = "Apt tray notifier"
VERSION = "M4_APP_VERSION"
SHORT_DESC = f"{APP_NAME} {VERSION} Copyright (c) 2021 Tomas Åkesson"

SLEEP_INTERVAL_SEC = 60


m4_include(src/ui_about.py)
m4_include(src/ui_updates.py)


class ShowUpdates(QDialog):
    def __init__(self, updates_list):
        super().__init__()
        self.ui = Ui_UpdatesDialog()
        self.ui.setupUi(self)
        self.ui.but_close.clicked.connect(self.accept)
        self.ui.textBrowser.setText("\n".join(updates_list))


class About(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_AboutDialog()
        self.ui.setupUi(self)
        self.ui.but_about_close.clicked.connect(self.accept)


class SystemTrayIcon(QSystemTrayIcon):
    def __init__(self, parent, args):
        self.icon = QIcon.fromTheme("system-software-update")
        QSystemTrayIcon.__init__(self, self.icon, parent)
        self.parent_win = parent
        self.exec_command = args["exec"]
        self.setContextMenu(self.createMenu())
        self.activated.connect(self.onActivated)
        self.has_shown_message = False
        self.updates_list = []

        # Show the icon in ten seconds after start, regardless if there are
        # updates or not
        self.show_anyway = True
        QTimer.singleShot(10 * 1000, self.hideIcon)

        self.show()
        self.onTimer()

        # Start a timer which checks for updates every SLEEP_INTERVAL_SEC seconds
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.onTimer)
        self.timer.start(SLEEP_INTERVAL_SEC * 1000)

    def hideIcon(self):
        self.show_anyway = False
        self.onTimer()

    def createMenu(self):
        menu = QMenu(self.parent_win)

        self.item_exec = menu.addAction("Perform update")
        self.item_exec.triggered.connect(self.runUpdateCommand)
        self.item_exec.setIcon(self.icon)
        self.item_exec.setDisabled(True)

        self.item_show = menu.addAction("Show updates")
        self.item_show.triggered.connect(self.showUpdates)
        self.item_show.setIcon(QIcon.fromTheme("edit-find"))
        self.item_show.setDisabled(True)
        menu.addSeparator()

        item_about = menu.addAction("About")
        item_about.triggered.connect(self.about)
        item_about.setIcon(QIcon.fromTheme("help-about"))
        menu.addSeparator()

        item_quit = menu.addAction("Exit")
        item_quit.triggered.connect(lambda: sys.exit())
        item_quit.setIcon(QIcon.fromTheme("application-exit"))

        return menu

    def getMessage(self):
        if self.updates_list:
            return (
                f"{len(self.updates_list)} system updates are available.\n"
                "Click icon to install.\n"
                "Right-click for options."
            )
        else:
            return "No system updates are available.\nRight-click for options."

    def updateTooltip(self):
        self.setToolTip(self.getMessage())

    def updateMessageBox(self):
        self.showMessage("System updates", self.getMessage(), self.icon, 7000)

    def about(self):
        About().exec()

    def showUpdates(self):
        ShowUpdates(self.updates_list).exec()

    # Called when icon is clicked
    def onActivated(self, reason):
        # Don't run update on right click or double-click
        if reason == QSystemTrayIcon.Context:
            return
        if reason == QSystemTrayIcon.DoubleClick:
            return
        self.runUpdateCommand()

    def runUpdateCommand(self):
        runUpdateCommand(self.exec_command)

    # Called every x milliseconds
    def onTimer(self):
        self.updates_list = checkForUpdates()

        # show icon and tooltip (or not)
        if self.updates_list or self.show_anyway:
            self.updateTooltip()
            self.show()
        else:
            self.hide()

        # show message (or not)
        if self.updates_list and not self.has_shown_message:
            self.updateMessageBox()
            self.has_shown_message = True

        # enable/disable menu items
        if self.updates_list:
            self.item_show.setEnabled(True)
            self.item_exec.setEnabled(True)

        else:
            self.item_show.setDisabled(True)
            self.item_exec.setDisabled(True)


def checkForUpdates(exit_on_fail=False):
    updates_list = []
    try:
        check_command = "apt list -qq --upgradable"
        process = subprocess.Popen(
            check_command.split(' '),
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True
        )
        process.stdin.close()
        for line in process.stdout:
            line = line.strip()
            updates_list.append(line)
    except Exception as e:
        if exit_on_fail:
            raise Exception("Failed to run check command: "+str(e))

    # Test data
    # updates_list = [
    #     "libldap-2.4-2/stable 2.4.47+dfsg-3+deb10u6 amd64 [upgradable from: 2.4.47+dfsg-3+deb10u5]",
    #     "libldap-common/stable 2.4.47+dfsg-3+deb10u6 all [upgradable from: 2.4.47+dfsg-3+deb10u5]",
    #     "libzstd1/stable 1.3.8+dfsg-3+deb10u2 amd64 [upgradable from: 1.3.8+dfsg-3+deb10u1]",
    # ]

    return updates_list


def runUpdateCommand(command):
    if command:
        command = command.split()
    else:
        command = [
            "xterm",
            "-hold",
            "-T",
            "Install updates",
            "-geometry",
            "80x40",
            "-e",
            "sudo apt update ; sudo apt upgrade ; echo \"Finished. You may now close this window\"",
        ]

    try:
        subprocess.Popen(command)
    except Exception as e:
        QMessageBox.warning(
            None,
            APP_NAME,
            "Failed to run command: "+str(e)
        )


def parse_args():
    ap = argparse.ArgumentParser(description=SHORT_DESC)

    ap.add_argument(
        "-e",
        "--exec",
        required=False,
        help="Command to run when icon is clicked. Default is to run \"sudo apt upgrade\" in an xterm."
    )
    return vars(ap.parse_args())


def main():
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)  # If not set, about window closes app

    i = 0
    while i < 60:
        if QSystemTrayIcon.isSystemTrayAvailable():
            break
        time.sleep(1.0)
        i += 1
    if not QSystemTrayIcon.isSystemTrayAvailable():
        QMessageBox.warning(
            None,
            APP_NAME,
            "Error: No system tray detected. Exiting."
        )
        sys.exit(1)

    args = parse_args()

    try:
        checkForUpdates(True)
    except Exception as e:
        QMessageBox.warning(None, APP_NAME, "Error: "+str(e))
        sys.exit(1)

    w = QWidget()
    SystemTrayIcon(w, args)

    # This lets the user press CTRL-C to kill the program even when it sleeps
    timer = QTimer()
    timer.timeout.connect(lambda: None)
    timer.start(100)

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
