# Apt tray notifier

Shows a notification when Debian updates are available.

## Requirements

Python 3, Qt5, xterm.

## Installation

```
make install
```

## Usage

Add to your .xsession file:

```
apt-tray-notifier &
```

The default command to run when performing an upgrade is "sudo apt upgrade"
in an xterm. So you should add any users which should be able to perform an
upgrade to the "sudo" group.

If you want to use a different command, specify it with the --exec argument:

```
apt-tray-notifier --exec=my_apt_upgrade &
```

You should run `apt update` periodically for this to be useful. The Debian way
of doing this is to add a file `/etc/apt/apt.conf.d/10periodic` with the following contents:

```
APT::Periodic::Enable "1";
APT::Periodic::Update-Package-Lists "1";
```

## Author

Created by Tomas Åkesson <tomas@entropic.se>

See LICENSE for license details.

Project homepage: https://gitlab.com/entropic-software/apt-tray-notifier
