APP_NAME = apt-tray-notifier
TARGET = bin/$(APP_NAME)

VERSION:=$(shell cat VERSION)
ifeq ($(CI_COMMIT_SHORT_SHA),)
	CI_COMMIT_SHORT_SHA:=$(shell git rev-parse --short HEAD)
endif
COMPILE_DATE:=$(shell LC_TIME=C date +"%F %T %z")
SHORT_DATE:=$(shell LC_TIME=C date +"%Y%m%d")

$(TARGET): src/main.py src/ui_about.py src/ui_updates.py
	mkdir -p bin
	m4  -D M4_APP_VERSION="$(VERSION)" \
		-D M4_GIT_REVISION="$(CI_COMMIT_SHORT_SHA)" \
		-D M4_COMPILE_DATE="$(COMPILE_DATE)" \
		-P src/main.py >$(TARGET)
	chmod +x $(TARGET)

src/ui_about.py: src/ui_about.ui
	pyuic5 -o src/ui_about.py src/ui_about.ui

src/ui_updates.py: src/ui_updates.ui
	pyuic5 -o src/ui_updates.py src/ui_updates.ui

test: $(TARGET)
	python3 -m py_compile $(TARGET)

install: $(TARGET)
	mkdir -p /usr/local/bin/
	install -m 755 $(TARGET) /usr/local/bin

uninstall:
	rm -f /usr/local/bin/$(APP_NAME)

clean: deb-clean
	rm -rf bin
	rm -rf __pycache__
	rm -f src/ui_about.py
	rm -f src/ui_updates.py


# Debian packaging

DEB_VERSION = $(VERSION)-$(SHORT_DATE)
PKG_NAME = $(APP_NAME)
DEB_PKG_BULLSEYE = $(PKG_NAME)_$(DEB_VERSION)~bullseye_all.deb
DEB_PKG_BOOKWORM = $(PKG_NAME)_$(DEB_VERSION)~bookworm_all.deb

deb-pkg-bullseye: $(TARGET)
	./mkdebpkg.sh bullseye $(DEB_VERSION)
	mv debian/build/bullseye/$(PKG_NAME).deb debian/build/$(DEB_PKG_BULLSEYE)

deb-pkg-bookworm: $(TARGET)
	./mkdebpkg.sh bookworm $(DEB_VERSION)
	mv debian/build/bookworm/$(PKG_NAME).deb debian/build/$(DEB_PKG_BOOKWORM)

deb-pkg: deb-pkg-bookworm deb-pkg-bullseye


deb-repo-bullseye: deb-pkg-bullseye
	update-deb-repo.sh -f $(PWD)/debian/build/$(DEB_PKG_BULLSEYE) -r bullseye

deb-repo-bookworm: deb-pkg-bookworm
	update-deb-repo.sh -f $(PWD)/debian/build/$(DEB_PKG_BOOKWORM) -r bookworm

deb-repo: deb-repo-bookworm deb-repo-bullseye


deb-clean:
	rm -rf debian/build
