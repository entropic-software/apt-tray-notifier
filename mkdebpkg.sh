#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

if [[ -z "${2-}" ]]; then
    echo "Usage: $0 RELEASE_CODENAME APP_VERSION"
    exit 1
fi

DEB_RELEASE="${1-}"
DEB_VERSION="${2-}"

BUILD_PATH="debian/build/${DEB_RELEASE}"
APP_NAME="apt-tray-notifier"
PKG_NAME="apt-tray-notifier"
DEB_PATH="${BUILD_PATH}/${PKG_NAME}"

PS4=""
set -o xtrace

mkdir -p "${DEB_PATH}/usr/bin"

cp "bin/${APP_NAME}" "${DEB_PATH}/usr/bin"
chmod +x "${DEB_PATH}/usr/bin/${APP_NAME}"

mkdir -p "${DEB_PATH}/usr/share/doc/${PKG_NAME}"
cp LICENSE "${DEB_PATH}/usr/share/doc/${PKG_NAME}/copyright"
cp README.md "${DEB_PATH}/usr/share/doc/${PKG_NAME}/"

mkdir -p "${DEB_PATH}/DEBIAN"
chmod 755 "${DEB_PATH}/DEBIAN"

sed <"debian/src/control.${DEB_RELEASE}.tpl" "s/{VERSION}/${DEB_VERSION}/" >"${DEB_PATH}/DEBIAN/control"

dpkg-deb --root-owner-group --build "${DEB_PATH}"
